const fonts = {
  latoBlack: "Lato-Black",
  latoBlackItalic: "Lato-BlackItalic",
  latoBold: "Lato-Bold",
  latoBoldItalic: "Lato-BoldItalic",
  latoHairline: "Lato-Hairline",
  latoHairlineItalic: "Lato-HairlineItalic",
  latoHeavy: "Lato-Heavy",
  latoHeavyItalic: "Lato-HeavyItalic",
  latoItalic: "Lato-Italic",
  latoLight: "Lato-Light",
  latoLightItalic: "Lato-LightItalic",
  latoMedium: "Lato-Medium",
  latoMediumItalic: "Lato-MediumItalic",
  latoRegular: "Lato-Regular",
  latoSemibold: "Lato-Semibold",
  latoSemiboldItalic: "Lato-SemiboldItalic",
  latoThin: "Lato-Thin",
  latoThinItalic: "Lato-ThinItalic",
}

export default fonts
