import { StyleSheet } from "react-native"
import { R } from "../../res"

const styles = StyleSheet.create({
  priceText: { marginLeft: "auto", fontFamily: R.fonts.latoBlack },
  labelView: {
    backgroundColor: "green",
    padding: 2,
    paddingHorizontal: 8,
    borderRadius: 3,
  },
  labelText: { color: "white" },
})

export default styles
