import React from "react"
import { View, Text } from "react-native"
import styles from "./styles"

export interface CardHeaderProps {
  onPress?: () => void
  style?: object
  label?: string
  price?: string
}

const CardHeader: React.SFC<CardHeaderProps> = (props: CardHeaderProps) => (
  <View style={props.style}>
    <View style={styles.labelView}>
      <Text style={styles.labelText}>{props.label}</Text>
    </View>
    <Text style={styles.priceText}>{"$" + (props.price === undefined ? "" : props.price)}</Text>
  </View>
)

export default CardHeader
