import React from "react"
import { TouchableOpacity, Text } from "react-native"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

export interface ListButtonProps {
  onPress?: () => void
  style?: object
  title?: string
}

const ListButton: React.SFC<ListButtonProps> = (props: ListButtonProps) => (
  <TouchableOpacity style={props.style}>
    <Text style={{ marginHorizontal: 5 }}>{props.title}</Text>
    <MaterialCommunityIcons
      name="chevron-down"
      size={30}
      color={"black"}
      style={{ marginLeft: "auto" }}
    ></MaterialCommunityIcons>
  </TouchableOpacity>
)

export default ListButton
