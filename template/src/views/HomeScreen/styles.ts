import { StyleSheet, Platform } from "react-native"
import { getWindowWidth } from "../../utils"
import { R } from "../../res"
let IMAGE_MARGIN = 40

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    elevation: 2,
    flex: 1,
    flexDirection: "column",
    alignSelf: "stretch",
    margin: 20,
    alignItems: "center",
    justifyContent: "flex-start",
    paddingBottom: 10,
    marginBottom: 15,
  },
  imageContainer: {
    flex: 1,
    ...Platform.select({
      ios: {
        shadowColor: "grey",
        shadowOffset: {
          height: 1,
          width: 0,
        },
        shadowRadius: 4,
        shadowOpacity: 0.5,
      },
    }),
    borderRadius: 2,
  },
  image: {
    borderRadius: 10,
    width: getWindowWidth() - IMAGE_MARGIN,
    height: (getWindowWidth() - IMAGE_MARGIN) * 0.6,
    resizeMode: "stretch",
  },
  card: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: -40,
    padding: 5,
    elevation: 2,
    borderRadius: 10,
    width: getWindowWidth() - IMAGE_MARGIN - 20,
    justifyContent: "center",
    backgroundColor: "white",
    alignItems: "center",
    flexDirection: "column",
    ...Platform.select({
      ios: {
        shadowColor: "grey",
        shadowOffset: {
          height: 1,
          width: 0,
        },
        shadowRadius: 4,
        shadowOpacity: 0.5,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  shareIcon: {
    position: "absolute",
    right: 10,
    top: 10,
  },
  search: {
    height: 30,
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 5,
    borderColor: "grey",
    borderRadius: 2,
    borderWidth: 1,
  },
  textInput: {
    flex: 0.5,
    height: 30,
    borderColor: "grey",
    borderRadius: 2,
    margin: 5,
    borderWidth: 1,
  },
  header: {
    flex: 1,
    padding: 5,
    flexDirection: "row",
    alignSelf: "stretch",
  },
  listButton: {
    flex: 0.3,
    height: 30,
    margin: 5,
    borderColor: "grey",
    borderRadius: 2,
    borderWidth: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  nameView: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch",
    marginLeft: 15,
    margin: 2,
  },
  nameText: { fontFamily: R.fonts.latoBold },
  priceText: { marginLeft: "auto", fontFamily: R.fonts.latoBlack },
  labelView: {
    backgroundColor: "green",
    padding: 2,
    paddingHorizontal: 8,
    borderRadius: 3,
  },
  labelText: { color: "white" },
  descriptionView: {
    flex: 1,
    marginLeft: 15,
    flexDirection: "row",
    alignSelf: "stretch",
    marginEnd: 5,
    alignItems: "center",
  },
  descriptionText: { color: "grey", textAlignVertical: "center" },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  button: {},
})

export default styles
