import React from "react"
import {
  Text,
  View,
  FlatList,
  Image,
  SafeAreaView,
  InteractionManager,
  TextInput,
  TouchableOpacity,
} from "react-native"
import { NavigationScreenProps } from "react-navigation"
import { connect } from "react-redux"
import { routes } from "../../navigation"
import { getProperties } from "../../state/ducks/PropertyDucks"
import styles from "./styles"
import Ionicons from "react-native-vector-icons/Ionicons"
import Entypo from "react-native-vector-icons/Entypo"
import ListButton from "../ListButton"
import CardHeader from "../CardHeader"
import { IProperty } from "../../utils"

interface IHomeScreenProps extends NavigationScreenProps {
  getProperties: () => {}
  properties: IProperty[]
}

class HomeScreen extends React.Component<IHomeScreenProps> {
  public componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.getProperties()
    })
  }
  public render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <TextInput style={styles.search} onChangeText={this.onChangeText} />
        <View style={{ flexDirection: "row", marginHorizontal: 15 }}>
          <TextInput style={styles.textInput} onChangeText={this.onChangeText} />
          <ListButton title="1bl" style={styles.listButton}></ListButton>
          <ListButton title="1br" style={styles.listButton}></ListButton>
        </View>
        {this.renderProperties(this.props.properties)}
      </SafeAreaView>
    )
  }

  private onChangeText = (value: string) => {}

  private renderProperties(properties: IProperty[]) {
    return (
      <FlatList<IProperty>
        data={properties}
        keyExtractor={(item: IProperty, index: number) => item.id}
        renderItem={this.renderItem}
        style={styles.container}
        showsVerticalScrollIndicator={false}
      />
    )
  }

  private renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => {
          this.navigateToDetails(item)
        }}
      >
        <View style={styles.imageContainer}>
          <Entypo name="share-alternative" size={20} color={"grey"} style={styles.shareIcon} />
          <Image source={{ uri: item.image }} style={styles.image}></Image>
        </View>
        <View style={styles.card}>
          <CardHeader style={styles.header} label={item.label} price={item.price} />
          <View style={styles.nameView}>
            <Text style={styles.nameText}>{item.name}</Text>
          </View>
          <View style={styles.descriptionView}>
            <Text style={styles.descriptionText}>{item.description}</Text>
            <Ionicons
              name="md-heart-empty"
              size={30}
              color={"black"}
              style={{ marginLeft: "auto" }}
            ></Ionicons>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  private navigateToDetails = (item: IProperty) =>
    this.props.navigation.navigate(routes.DetailsScreen, { item })
}

const mapStateToProps = ({ property }) => ({
  properties: property.properties,
})

const mapDispatchToProps = dispatch => ({
  getProperties: () => dispatch(getProperties()),
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
