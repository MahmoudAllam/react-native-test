import React from "react"
import { Text, View, Image, TouchableOpacity } from "react-native"
import { NavigationScreenProps } from "react-navigation"
import { connect } from "react-redux"
import styles from "./styles"
import Ionicons from "react-native-vector-icons/Ionicons"
import Entypo from "react-native-vector-icons/Entypo"
import CardHeader from "../CardHeader"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import Tabs from "react-native-tabs"

interface IDetailsScreenProps extends NavigationScreenProps {}

interface IProperty {
  id: string
  name: string
  price: Number
  label: string
  description: string
}

interface IDetailsScreenState {
  page: string
}

class DetailsScreen extends React.Component<IDetailsScreenProps, IDetailsScreenState> {
  constructor(props) {
    super(props)
    this.state = { page: "details" }
  }

  public render() {
    let item = this.props.navigation.getParam("item")
    return (
      <View style={styles.container}>
        <Image source={{ uri: item.image }} style={styles.image}></Image>
        {this.renderCard(item)}
        {this.renderBackButton()}
      </View>
    )
  }

  private renderCard = item => {
    return (
      <View style={styles.card}>
        <CardHeader style={styles.header} label={item.label} price={item.price} />
        {this.renderDescription(item)}
        {this.renderDetailsView(item)}
      </View>
    )
  }

  private renderDetailsView = item => {
    return (
      <View style={styles.detailsContainer}>
        <View style={styles.detailsView}>
          <Text>{item[this.state.page]}</Text>
        </View>
        {this.renderTab()}
      </View>
    )
  }

  private renderDescription = item => {
    return (
      <View style={styles.descriptionView}>
        <Text style={styles.descriptionText}>{item.description}</Text>
        <Ionicons
          name="md-heart-empty"
          size={30}
          color={"black"}
          style={{ marginLeft: "auto" }}
        ></Ionicons>
      </View>
    )
  }
  private renderTab = () => {
    return (
      <Tabs
        selected={this.state.page}
        style={styles.unSelectedTab}
        onSelect={el => this.setState({ page: el.props.name })}
      >
        <Text name="details" selectedIconStyle={styles.selectedTab}>
          {"Details"}
        </Text>
        <Text name="amenities" selectedIconStyle={styles.selectedTab}>
          {"Amenities"}
        </Text>
        <Text name="rooms" selectedIconStyle={styles.selectedTab}>
          {"Rooms"}
        </Text>
      </Tabs>
    )
  }
  private renderBackButton = () => {
    return (
      <View style={styles.backButtonView}>
        <TouchableOpacity
          onPress={() => {
            requestAnimationFrame(this.navigateToHome)
          }}
        >
          <MaterialCommunityIcons
            name="chevron-left"
            size={40}
            color={"black"}
          ></MaterialCommunityIcons>
        </TouchableOpacity>

        <Entypo name="share-alternative" size={30} color={"grey"} style={{ marginLeft: "auto" }} />
      </View>
    )
  }

  private navigateToHome = () => this.props.navigation.goBack()
}

const mapStateToProps = ({ property }) => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen)
