import { StyleSheet, Platform } from "react-native"
import { getWindowWidth } from "../../utils"
import { R } from "../../res"
import { isIphoneXorAbove } from "../../utils"

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: "center", alignItems: "center" },
  itemContainer: {
    flex: 1,
    flexDirection: "column",
    alignSelf: "stretch",
    margin: 20,
    alignItems: "center",
    justifyContent: "flex-start",
    paddingBottom: 10,
    marginBottom: 15,
  },
  unSelectedTab: { backgroundColor: "white", borderTopWidth: 2, borderTopColor: "grey" },
  selectedTab: { borderTopWidth: 2, borderTopColor: "blue" },
  image: {
    width: getWindowWidth(),
    height: getWindowWidth() * 0.6,
    resizeMode: "stretch",
  },
  detailsView: { flex: 1, marginLeft: 15 },
  detailsContainer: {
    flexDirection: "row",
    flex: 1,
    alignSelf: "stretch",
  },
  card: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: -40,
    marginBottom: 20,
    padding: 5,
    borderRadius: 10,
    width: getWindowWidth() - 20,
    justifyContent: "center",
    backgroundColor: "white",
    alignItems: "center",
    flexDirection: "column",
    ...Platform.select({
      ios: {
        shadowColor: "grey",
        shadowOffset: {
          height: 1,
          width: 0,
        },
        shadowRadius: 4,
        shadowOpacity: 0.5,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  shareIcon: {
    position: "absolute",
    right: 10,
    top: 10,
  },
  search: {
    height: 30,
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 5,
    borderColor: "grey",
    borderRadius: 2,
    borderWidth: 1,
  },
  textInput: {
    flex: 0.5,
    height: 30,
    borderColor: "grey",
    borderRadius: 2,
    margin: 5,
    borderWidth: 1,
  },
  header: {
    padding: 5,
    alignSelf: "stretch",
    flexDirection: "row",
    height: 30,
  },
  listButton: {
    flex: 0.3,
    height: 30,
    margin: 5,
    borderColor: "grey",
    borderRadius: 2,
    borderWidth: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  nameView: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch",
    marginLeft: 15,
    margin: 2,
  },
  nameText: { fontFamily: R.fonts.latoBold },
  priceText: { marginLeft: "auto", fontFamily: R.fonts.latoBlack },
  labelView: {
    backgroundColor: "green",
    padding: 2,
    paddingHorizontal: 8,
    borderRadius: 3,
  },
  labelText: { color: "white" },
  descriptionView: {
    // flex: 1,
    marginLeft: 15,
    flexDirection: "row",
    alignSelf: "stretch",
    marginEnd: 5,
    // backgroundColor: "red",
    alignItems: "center",
  },
  backButtonView: {
    flex: 1,
    flexDirection: "row",
    position: "absolute",
    top: isIphoneXorAbove() ? 40 : 20,
    left: 10,
    right: 20,
  },
  descriptionText: { color: "black", textAlignVertical: "center" },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
})

export default styles
