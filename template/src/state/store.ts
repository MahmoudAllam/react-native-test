import Logger from "redux-logger"
import { persistReducer, persistStore } from "redux-persist"
import createSagaMiddleware from "redux-saga"
import { configureStore } from "redux-starter-kit"
import Reactotron from "../services/reactotron"
import { IS_DEV_ENV } from "../utils"
import intialState from "./initialState"
import persistConfig from "./persistConfig"
import rootReducer from "./rootReducer"
import rootSaga from "./rootSaga"

const configureAppStore = () => {
  // Reactotron & Saga configuration
  const sagaMiddleware = createSagaMiddleware({})

  const appStore = configureStore({
    reducer: persistReducer(persistConfig, rootReducer),
    middleware: IS_DEV_ENV ? [sagaMiddleware, Logger] : [sagaMiddleware],
    devTools: false,
    preloadedState: intialState,
  })

  sagaMiddleware.run(rootSaga)
  return appStore
}

const store = configureAppStore()
const persistor = persistStore(store)

export { store, persistor }
