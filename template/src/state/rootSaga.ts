import { all, call } from "redux-saga/effects"
import { propertyStateSubscription } from "./ducks/PropertyDucks"

export default function* rootSaga() {
  yield all([call(propertyStateSubscription) /*, call(sharedASubscription)*/])
}
