import { put, takeLatest } from "redux-saga/effects"
import data from "../../services/Data"
import { IProperty } from "../../utils"
enum PropertyState {
  loading = "loading",
  failed = "failed",
  empty = "empty",
  success = "success",
}

enum PropertyActionType {
  getProperties = "property/get_properties",
  setProperties = "property/set_properties",
  setState = "property/set_state",
  setError = "property/set_error",
}

export interface getPropertiesAction {
  payload: {}
  type: PropertyActionType.getProperties
}

export interface setPropertiesAction {
  payload: {
    properties: IProperty[]
  }
  type: PropertyActionType.setProperties
}
export interface setPropertyErrorAction {
  payload: {
    error: string
  }
  type: PropertyActionType.setError
}
export interface setPropertyStateAction {
  payload: {
    propertyState: PropertyState
  }
  type: PropertyActionType.setState
}

// Action Creators
export const getProperties = (): getPropertiesAction => ({
  payload: {},
  type: PropertyActionType.getProperties,
})

export const setState = (propertyState: PropertyState): setPropertyStateAction => ({
  payload: {
    propertyState,
  },
  type: PropertyActionType.setState,
})

export const setError = (error: string): setPropertyErrorAction => ({
  payload: {
    error,
  },
  type: PropertyActionType.setError,
})

export const setProperties = (properties: IProperty[]): setPropertiesAction => ({
  payload: {
    properties,
  },
  type: PropertyActionType.setProperties,
})

// Reducer
interface IPropertState {
  properties: IProperty[]
  propertyState: PropertyState
  error?: string
}

const initialState: IPropertState = {
  properties: [],
  propertyState: PropertyState.empty,
  error: undefined,
}

export default function(
  state = initialState,
  action: { type: PropertyActionType; payload: Record<string, any> }
): IPropertState {
  switch (action.type) {
    case PropertyActionType.setProperties:
      let { properties } = action.payload
      return { ...state, properties }
    case PropertyActionType.setState:
      let { propertyState } = action.payload
      return { ...state, propertyState }
    case PropertyActionType.setError:
      let { error } = action.payload
      return { ...state, error }
    default:
      return state
  }
}

// Sagas
function* getPropertiesSaga({ payload }: getPropertiesAction) {
  yield put(setState(PropertyState.loading))
  yield put(setProperties(data))
  yield put(setState(PropertyState.success))
}

export function* propertyStateSubscription() {
  yield takeLatest(PropertyActionType.getProperties, getPropertiesSaga)
}
