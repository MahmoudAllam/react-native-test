import { combineReducers } from "redux"
import propertyReducer from "./ducks/PropertyDucks"

export default combineReducers({
  property: propertyReducer,
})
