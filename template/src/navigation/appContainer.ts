import { createAppContainer, createStackNavigator } from "react-navigation"
import { IS_IOS } from "../utils"
import * as Screens from "../views"
import routes from "./routes"

const appNavigator = createStackNavigator(
  {
    [routes.HomeScreen]: {
      screen: Screens.HomeScreen,
    },
    [routes.DetailsScreen]: {
      screen: Screens.DetailsScreen,
    },
  },
  {
    // StackNavigatorConfig
    initialRouteName: routes.HomeScreen,
    headerMode: "none",
  }
)

export default createAppContainer(appNavigator)
