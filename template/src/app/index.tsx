import React, { Component } from "react"
import { Provider } from "react-redux"
import { PersistGate } from "redux-persist/lib/integration/react"
import { AppContainer } from "../navigation"
import { persistor, store } from "../state/store"

export default class App extends Component {
  public componentDidMount() {}

  public render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppContainer />
        </PersistGate>
      </Provider>
    )
  }
}
