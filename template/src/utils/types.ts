export interface IProperty {
  id: string
  name: string
  price: Number
  label: string
  description: string
  image: string
  details
  amenities
  rooms
}
